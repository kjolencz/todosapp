package edu.towson.cosc431.olencz.todos

import android.app.Activity
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.content.Intent
import androidx.core.app.ComponentActivity.ExtraData
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import android.widget.EditText
import edu.towson.cosc431.olencz.todos.models.Todo
import kotlinx.android.synthetic.main.todo_view.*
import com.google.gson.Gson

class NewTodoActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_todo)

        val saveBtn: Button = this.findViewById(R.id.saveButton)
        val titleText = findViewById<EditText>(R.id.titleText)
        val contentsText = findViewById<EditText>(R.id.textContents)

        saveBtn.setOnClickListener(){
            var title = titleText.text.toString()
            var contents = contentsText.text.toString()
            val intent = Intent(this@NewTodoActivity, MainActivity::class.java)
            intent.putExtra("Title", title)
            intent.putExtra("Contents",contents)
            startActivity(intent)
        }
    }

    private fun handleSaveClick() {
        val intent = Intent()

        val todo = Todo(
            title = todo_title.editableText.toString(),
            contents = todo_content.editableText.toString(),
            isChecked = todo_checkbox.isChecked
        )

        val json: String = Gson().toJson(todo)

        intent.putExtra(TODO_KEY, json)

        setResult(Activity.RESULT_OK, intent)

        finish()
    }
    companion object {
        val TODO_KEY = "TODO_EXTRA"
    }
}
