package edu.towson.cosc431.olencz.todos

import android.text.Editable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import edu.towson.cosc431.olencz.todos.interfaces.ITodoController
import edu.towson.cosc431.olencz.todos.models.Todo
import kotlinx.android.synthetic.main.activity_new_todo.view.*

class TodoAdapter(private val controller: ITodoController) : RecyclerView.Adapter<TodoViewHolder>(){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TodoViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.todo_view, parent, false)
        val viewHolder = TodoViewHolder(view)

        view.checkBoxCompleted.setOnClickListener{
            val position = viewHolder.adapterPosition
            controller.isChecked(position)
            this.notifyItemChanged(position)

        }

        return viewHolder
    }

    override fun getItemCount(): Int {
        return controller.todos.getCount()
    }

    override fun onBindViewHolder(holder: TodoViewHolder, position: Int) {
        val todo = controller.todos.getTodo(position)
        holder.bindTodo(todo)
    }
}

class TodoViewHolder(view: View): RecyclerView.ViewHolder(view){
    fun bindTodo(todo: Todo){
        itemView.titleText.text = Editable.Factory.getInstance().newEditable(todo.title)
        itemView.textContents.text = Editable.Factory.getInstance().newEditable(todo.contents)
        itemView.checkBoxCompleted.isChecked= todo.isChecked
    }
}