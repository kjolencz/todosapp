package edu.towson.cosc431.olencz.todos
import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log
import edu.towson.cosc431.olencz.todos.models.Todo

class DBHelper(context: Context) : SQLiteOpenHelper(context, DB_NAME, null, DB_VERSION) {
    override fun onCreate(db: SQLiteDatabase?) {
        val CREATE_TABLE = "CREATE TABLE $TABLE_NAME (" +
                title + " INTEGER PRIMARY KEY," +
                contents + " TEXT," + contents + " TEXT," +
                isChecked + " TEXT);"
        db?.execSQL(CREATE_TABLE)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        val DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME
        db?.execSQL(DROP_TABLE)
        onCreate(db)
    }

    companion object {
        private val DB_VERSION = 1
        private val DB_NAME = "MyTODOS"
        private val TABLE_NAME = "TODOS"
        private val title = "Title"
        private val contents = "Contents"
        private val isChecked = "isChecked"
    }

    fun addTodo(todos: Todo): Boolean {
        val db = this.writableDatabase
        val values = ContentValues()
        values.put(title, todos.title)
        values.put(contents, todos.contents)
        values.put(isChecked, todos.isChecked)
        val _success = db.insert(TABLE_NAME, null, values)
        db.close()
        return (Integer.parseInt("$_success") != -1)
    }

    val todo: List<Todo>
        get() {
            val taskList = ArrayList<Todo>()
            val db = writableDatabase
            val selectQuery = "SELECT  * FROM $TABLE_NAME"
            val cursor = db.rawQuery(selectQuery, null)
            if (cursor != null) {
                cursor.moveToFirst()
                while (cursor.moveToNext()) {

                }
            }
            cursor.close()
            return taskList
        }

    fun updateTodo(todos: Todo): Boolean {
        val db = this.writableDatabase
        val values = ContentValues()
        values.put(title, todos.title)
        values.put(contents, todos.contents)
        values.put(isChecked, todos.isChecked)
        val _success = db.update(TABLE_NAME, values, ID + "=?", arrayOf(todos.id.toString())).toLong()
        db.close()
        return Integer.parseInt("$_success") != -1
    }

    fun deleteTodo(_id: Int): Boolean {
        val db = this.writableDatabase
        val _success = db.delete(TABLE_NAME, ID + "=?", arrayOf(_id.toString())).toLong()
        db.close()
        return Integer.parseInt("$_success") != -1
    }
}