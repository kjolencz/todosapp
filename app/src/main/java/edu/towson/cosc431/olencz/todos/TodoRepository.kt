package edu.towson.cosc431.olencz.todos

import edu.towson.cosc431.olencz.todos.interfaces.ITodoRepository
import edu.towson.cosc431.olencz.todos.models.Todo



class TodoRepository : ITodoRepository {
    private var todos: MutableList<Todo> = mutableListOf()

    init {
        val seed = (1..10).map { idx -> Todo("Todo${idx}", "Content${idx}", true)}
        todos.addAll(seed)
    }

    override fun addTodo(todo: Todo) {
        todos.add(todo)
    }

    override fun getCount(): Int {
        return todos.size
    }

    override fun getTodo(idx: Int): Todo {
        return todos.get(idx)
    }

    override fun getAll(): List<Todo> {
        return todos
    }

    override fun remove(todo: Todo) {
        todos.remove(todo)
    }

    override fun replace(idx: Int, todo: Todo) {
        if(idx >= todos.size) throw Exception("Outside of bounds")
        todos[idx] = todo
    }
}
