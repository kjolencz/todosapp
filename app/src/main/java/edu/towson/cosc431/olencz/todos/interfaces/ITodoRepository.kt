package edu.towson.cosc431.olencz.todos.interfaces

import edu.towson.cosc431.olencz.todos.models.Todo

interface ITodoRepository {
    fun getCount(): Int
    fun getTodo(idx: Int): Todo
    fun getAll(): List<Todo>
    fun remove(todo: Todo)
    fun addTodo(todo:Todo)
    fun replace(idx: Int, todo: Todo)
}