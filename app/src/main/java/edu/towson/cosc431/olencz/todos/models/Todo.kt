package edu.towson.cosc431.olencz.todos.models

data class Todo (
    val title: String,
    val contents: String,
    val isChecked: Boolean
)