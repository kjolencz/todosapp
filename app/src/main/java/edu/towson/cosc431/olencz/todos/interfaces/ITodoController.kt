package edu.towson.cosc431.olencz.todos.interfaces

interface ITodoController {
    fun deleteTodo(idx:Int)
    fun isChecked(idx:Int)
    fun launchNewScreen()
    val todos: ITodoRepository
    fun launchNewTodoScreen()
}