package edu.towson.cosc431.olencz.todos

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import edu.towson.cosc431.olencz.todos.interfaces.ITodoController
import edu.towson.cosc431.olencz.todos.interfaces.ITodoRepository
import edu.towson.cosc431.olencz.todos.models.Todo
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_new_todo.*
import kotlinx.android.synthetic.main.activity_todos.*

class MainActivity : AppCompatActivity(), ITodoController {
    override fun launchNewTodoScreen() {
        val intent = Intent(this, NewTodoActivity::class.java)
        startActivityForResult(intent, ADD_TODO_REQUEST_CODE)
    }

    override fun launchNewScreen() {
        val intent = Intent(this, NewTodoActivity::class.java)
        startActivityForResult(intent, ADD_TODO_REQUEST_CODE)
    }


    override fun deleteTodo(idx: Int) {
        val current = todos.getTodo(idx)
        todos.remove(current)
    }

    override fun isChecked(idx: Int) {
        val todo = todos.getTodo(idx)
        val newTodo = todo.copy(isChecked= !todo.isChecked)
        todos.replace(idx,newTodo)
    }

    override lateinit var todos: ITodoRepository

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        todos = TodoRepository()

        val adapter = TodoAdapter(this)


        recyclerView.adapter = adapter

        recyclerView.layoutManager = LinearLayoutManager(this)



        saveButton.setOnClickListener { launchNewTodoScreen() }


    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when(requestCode) {
            ADD_TODO_REQUEST_CODE -> {
                when(resultCode) {
                    Activity.RESULT_OK -> {
                        // handle the result
                        val json = data?.getStringExtra(NewTodoActivity.TODO_KEY)

                        val todo = Gson().fromJson(json, Todo::class.java)

                        todos.addTodo(todo)
                    }
                    Activity.RESULT_CANCELED -> {
                        Toast.makeText(this, "Use cancelled", Toast.LENGTH_SHORT).show()
                    }
                }
            }
        }
    }

    companion object {
        val ADD_TODO_REQUEST_CODE = 1
    }

}